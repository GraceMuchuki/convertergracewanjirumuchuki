package com.example.Grace.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        millimetres = findViewById(R.id.millimetres);
        inches = findViewById(R.id.inches);
    }
      public void Convert(View view) {

        input = millimetres.getText().toString();
        Double inches2 = Double.parseDouble(input)/25.4;
        inches.setText(inches2.toString());
    }

    public void exit(View view) {
        finish();
    }
}
